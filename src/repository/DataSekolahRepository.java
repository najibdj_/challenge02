package repository;

import java.util.Map;

/***
 * Repository for DataSekolah
 */
public interface DataSekolahRepository {
    Map<Integer, Integer> groupAll();

    Map<Integer, Integer> showLessThan(int input);

    String writeLessThan(int input);

    Map<Integer, Integer> showMoreThan(int input);

    String writeMoreThan(int input);

    String getValueOf(int input);

    double calcMean();

    int calcMode();

    double calcMedian();
}
